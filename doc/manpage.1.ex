.SH NAME

treeplot - Phylogenetic tree file converter

.SH SYNOPSIS

.B treeplot [options]

.I input output

.B ["

.I -yy -zz

.B ..."]

.SH AVAILABILITY

All UNIX flavors

.SH DESCRIPTION

Treeplot is a conversion tool, from "Phylip" phylogenetic tree file to
Postscript (.ps), Adobe Illustrator (.ai), Scalable Vector Graphic (.svg),
Computer Graphic Metafile(.cgm), Hewlet Packard Graphic Language (.hpgl), xfig
file (.fig), gif image file(.gif), PBM Portable aNy Map file (.pnm)

.SH OPTIONS

There are no options, but we'll make some up.

.TP 5

-yy

is one option

.TP

-zz

is another option

.SH AUTHOR

Olivier Langella