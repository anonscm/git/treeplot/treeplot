 /***************************************************************************
                          internat.h  -  description
                             -------------------
    begin                : Thu Sep 21 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//internationalisation
#ifndef INTERNAT_H
# define INTERNAT_H
# ifdef COMPILATION_MINGW32
//#  ifndef RIEN
//#   define RIEN
//char * rien(char * truc) {return (truc);};
#  define _(String) (String)
#  define textdomain(String) (String)
//#  endif
//#  define _(String) rien(String)
//
# else
#  ifndef _LIBINTL_H
#   include <libintl.h>
#   define _(String) gettext (String)
#  endif
# endif
#endif


