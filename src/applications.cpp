/***************************************************************************
                          applications.cpp  -  Librairie d'objets pour creer des applications
                             -------------------
    begin                : ven aug 14 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include"applications.h"

void Application::affPubSortie() const {

		cout << endl << endl;
		cout << "Programmation: Olivier Langella" << endl;
		cout << "CNRS UPR9034 Laboratoire PGE" << endl;
		cout << "e-mail: langella@pge.cnrs-gif.fr" << endl;
		cout << "Web: http://www.pge.cnrs-gif.fr/bioinfo" << endl;
		cout << endl << endl;
}

void Application::affPubEntree() const {

		cout << endl << endl;
		cout << "**************************************" << endl;
	//	cout << "*  Populations 1.1.00  CNRS UPR9034  *" << endl;
		cout << "*      langella@pge.cnrs-gif.fr      *" << endl;
		cout << "* http://www.pge.cnrs-gif.fr/bioinfo *" << endl;
		cout << "**************************************" << endl;
		cout << endl << endl;
}


void Application::lancement(int nbcommandes, char ** commandes) {

	if (nbcommandes > 1) {
		fLigneCommande(commandes, nbcommandes);
	}
	else {
		affPubEntree();

		while (menu_principal() != 0) {}

		affPubSortie();
	}
}

void Application::fLigneCommande(char ** commandes, int nbcommandes) {
	affPubEntree();
	
}

int Application::menu_principal() {
	int choix;
	cout << endl << endl;
	cout << "0) Quitter" << endl;

/*	cout << endl << "choix: ";
	cin >> choix;
*/
	choix = DemandeChoix(0,0);

	switch (choix) {
	case 0:
		return(0);
		break;
//	case 1:
//		return(1);
//		break;
	default:
		return(0);
		break;
	}
}

int Application::menu_formatMatrice() {
	int choix;
	cout << endl << endl;
	cout << _("Matrix outputfile format:") << endl;
	cout << "1) " << _("ASCII, (excell or gnumeric compliant)") << endl;
	cout << "2) " << _("NtSys") << endl;
	cout << "3) " << _("Phylip") << endl;
	cout << "4) " << _("xgobi") << endl;

/*	cout << endl << "choix: ";
	cin >> choix;
*/
	choix = DemandeChoix(1,4);

	switch (choix) {
	case 1:
		_formatMatrice = 1;
		return(1);
		break;
	case 2:
		_formatMatrice = 2;
		return(2);
		break;
	case 3:
		_formatMatrice = 4;
		return(3);
		break;
	case 4:
		_formatMatrice = 3;
		return(4);
		break;
	default:
		return(0);
		break;
	}
}

bool Application::litMatriceLD(MatriceLD &mat) {
	bool ok(false);
	
/*	if (*PPmat != NULL) {
		delete *PPmat;
		*PPmat = NULL;
	}*/

	while (_fichier.is_open() == 0) {
		cout << "Nom du fichier contenant la matrice ?" << endl;
		cin >> _nomFichier;

		_fichier.open(_nomFichier.c_str(), ios::in);
		_fichier.clear();
	}

	try {
	//	*PPmat = new MatriceLD();
		cout << "Lecture en cours..."<< endl;
		mat.iFichier(_fichier);
		_fichier.clear();
		_fichier.close();
		ok = true;
	}
	catch (MatriceLD::Anomalie pb){
		_fichier.close();
		ok = false;
	//	if (*PPmat != NULL) delete *PPmat;

		switch (pb.le_pb) {
		default:
			cout << "Le format de ce fichier semble incorrect..." << endl;
			break;
		}
	}

	return(ok);
}

bool Application::ecritMatriceLD(MatriceLD &mat,string nomFichier, int format) {
	bool ok(false);
	
	if (format == 0) format = _formatMatrice;

/*	if (*PPmat != NULL) {
		return (false);
	}*/
//cerr << format;
	_sortie << ends;
	_sortie.close();

	while (nomFichier == "") {
		cout << "Nom du fichier contenant la matrice � �crire ?" << endl;
		cin >> nomFichier;
	}

	_sortie.open(nomFichier.c_str(), ios::out);

	try {
		mat.ofFormat(_sortie, format, nomFichier);
		_sortie << ends;
		_sortie.close();
		ok = true;
	}
	catch (MatriceLD::Anomalie pb){
		_sortie.close();
		ok = false;

		switch (pb.le_pb) {
		default:
			cout << _("Error writing matrix") << " MatriceLD::Anomalie " << pb.le_pb << endl;
			break;
		}
	}

	return(ok);
}


bool Application::DemandeOuiNon(char laquestion[]) const{
	string rep;

	cout << laquestion << endl;
	cin >> rep;
//	cout << endl;
	if (rep == "") rep.assign("N");

	switch (rep[0]) {
	case 'Y':
		return(true);
		break;
	case 'y':
		return(true);
		break;
	case 'o':
		return(true);
		break;
	case 'O':
		return(true);
		break;
	default:
		return (false);
		break;
	}
	return (false);
}


string Application::DemandeString(char laquestion[]) const{
	string rep("");

	while (rep == "") {
		cout << endl << laquestion << endl;
		cin >> rep;
	}
	return (rep);
}

string Application::DemandeFichier(char laquestion[]) {
	string rep("");
	bool ok(false);

	while ((rep == "") || (ok == false)) {
		ok = false;
		cout << endl << laquestion << endl;
		cin >> rep;
		if ((rep != "") && (_confirmeEcraseFichier) && (fFichierExiste(rep.c_str()))) {		
			cout << _("The file ") << rep << _(" already exist.") << endl;
			if (DemandeOuiNon(_("Choose Y if you to overwrite it: "))) ok = true;
		}
		else ok = true;
	}
	
	return (rep);	
}

float Application::DemandeReel(char laquestion[], float inf, float sup) const{
	float reel(-9999);
	string rep;

	while ((reel < inf) || (reel > sup)) {
		cout << endl << laquestion << endl;
		cin >> rep;
		reel = atof(rep.c_str());
	}
	return (reel);
}

int Application::DemandeEntier(char laquestion[], int inf, int sup) const{
	int entier(-999);
	string rep;

	while ((entier < inf) || (entier > sup)) {
		cout << endl << laquestion << endl;
		cin >> rep;
		entier = atoi(rep.c_str());
	}
	return (entier);
}

int Application::DemandeEntier(char laquestion[], int inf, int sup, int defaut) const{
	int entier(-999);
	char car;
	string mot;
	if ((defaut < inf) || (defaut > sup)) cerr << "DemandeEntier defaut ERROR" << endl;

	while ((entier < inf) || (entier > sup)) {
		cout << endl << laquestion << endl;
		cin.get(car);
		cin.get(car);
//	rep[0] = rep[0]+50;
//		cout << rep;
		if (car == '\n') entier = defaut;	
		else {
			cin >> mot;
			mot = car + mot;
			entier = atoi(mot.c_str());
		}
	}
	return (entier);
}


int Application::DemandeChoix(int inf, int sup) const {

	return(DemandeEntier(_("Your choice: "),inf,sup));
}

bool Application::litJeuMatriceLD(JeuMatriceLD &jeumat) {
	bool ok(false);
	
/*	if (*PPmat != NULL) {
		delete *PPmat;
		*PPmat = NULL;
	}*/

	while (_fichier.is_open() == 0) {
		cout << "Nom du fichier contenant les matrices ?" << endl;
		cin >> _nomFichier;

		_fichier.open(_nomFichier.c_str(), ios::in);
		_fichier.clear();
	}

	try {
	//	*PPmat = new MatriceLD();
		cout << "Lecture en cours..."<< endl;
		jeumat.iFlux(_fichier);
		_fichier.clear();
		_fichier.close();
		ok = true;
	}
	catch (MatriceLD::Anomalie pb){
		_fichier.close();
		ok = false;
	//	if (*PPmat != NULL) delete *PPmat;

		switch (pb.le_pb) {
		default:
			cout << "Le format de ce fichier semble incorrect..." << endl;
			break;
		}
	}

	return(ok);
}

bool Application::ecritJeuMatriceLD(const JeuMatriceLD &jeumat, string nomFichier, int format) {
	bool ok(false);
	//string nomFichier;
	
	if (format == 0) format = _formatMatrice;

/*	if (*PPmat != NULL) {
		return (false);
	}*/
//cerr << format;
	_sortie << ends;
	_sortie.close();

	while (_sortie.is_open() == 0) {
		while (nomFichier == "") {
			cout << "Nom du fichier contenant les matrices � �crire ?" << endl;
			cin >> nomFichier;
		}
		_sortie.open(_nomFichier.c_str(), ios::out);
	}

	try {
		nomFichier = _nomFichier;
		jeumat.oFlux(_sortie, format);
		_sortie << ends;
		_sortie.close();
		ok = true;
	}
	catch (MatriceLD::Anomalie pb){
		_sortie.close();
		ok = false;

		switch (pb.le_pb) {
		default:
			cout << "Erreur d'ecriture..." << endl;
			break;
		}
	}

	return(ok);
}
/** test si nomfichier existe */
bool Application::fFichierExiste(const string & nomfichier){
	_fichier.open(nomfichier.c_str(),ios::in);

	if(_fichier.is_open() == 0){
		_fichier.clear();
		return (false);
	}
	_fichier.close();	
	return (true);
}
