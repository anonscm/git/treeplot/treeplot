/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : lun mar 26 11:11:09 CEST 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include "appltreeplot.h"

int main(int argc, char *argv[])
{
	ApplTreePlot application;

	application.lancement(argc, argv);

  return EXIT_SUCCESS;
}
