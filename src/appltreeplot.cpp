/***************************************************************************
                          appltreeplot.cpp  -  description
                             -------------------
    begin                : Tue Dec 19 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "appltreeplot.h"

ApplTreePlot::ApplTreePlot():flux_infos(cout){
	_Pdessin_arbre = 0;
}
ApplTreePlot::~ApplTreePlot(){
	if (_Pdessin_arbre != 0) delete _Pdessin_arbre;
}

void ApplTreePlot::affPubSortie() const {

	cout << endl << endl;
	cout << _("Development") << ": Olivier Langella" << endl;
	cout << "" << endl;
	cout << "e-mail: langella@moulon.inra.fr" << endl;
	cout << "" << endl;
	cout << endl << endl;
}

void ApplTreePlot::affPubEntree() const {

	cout << endl;
	cout << "Treeplot version " << version_treeplot << endl;
	cout << _("Developpment") << ": Olivier Langella" << endl;
	cout << "" << endl;
	cout << "e-mail: langella@moulon.inra.fr" << endl;
	cout << "" << endl;
	cout << endl;
}

void ApplTreePlot::fLiHelp() const {
	affPubEntree();
//	cout << "Usage: cp [OPTION]... SOURCE DESTINATION"<< endl;
	cout << "USAGE: treeplot SOURCE DESTINATION -[options]"<< endl;
	cout << "DESTINATION is a postscript file (output.ps) by default." << endl;
	cout << "to produce other format, choose one of these extensions:" << endl;
	cout << ".ps ==> Postscript" << endl;
	cout << ".ai ==> Adobe Illustrator" << endl;
	cout << ".svg ==> Scalable Vector Graphic" << endl;
	cout << ".cgm ==> Computer Graphic Metafile" << endl;
	cout << ".hpgl ==> Hewlet Packard Graphic Language" << endl;
	cout << ".fig ==> xfig file" << endl;
	cout << ".gif ==> gif image file" << endl;
	cout << ".pnm ==> PBM Portable aNy Map file" << endl;

	cout << endl << "OPTIONS for treeplot"<< endl;
	cout << "-t rect" << endl;
	cout << "drawing rectangular cladogram" << endl;
	cout << "-t phylogram" << endl;
	cout << "drawing phylogram" << endl;
	cout << "-nb" << endl;
	cout << "no bootstrap values" << endl;
	cout << "-og 'LIST'" << endl;
	cout << "-g 'COLOR' 'LIST'" << endl;
	cout << "where LIST is a list of OTU's (separated by spaces)" << endl;
	cout << "COLOR is a name of color (in english) or an rgb code (example: '255 0 0' for red)" << endl;

// BitmapPlotter -> 9
// MetaPlotter
// TekPlotter,
// ReGISPlotter
}

void ApplTreePlot::lancement(int nbcommandes, char ** commandes) {


	if (nbcommandes > 1) {
		fLigneCommande(commandes, nbcommandes);
	}
	else {
		fLiHelp();
	}

}

void ApplTreePlot::fLigneCommande(char ** commandes, int nbcommandes) {
//	affPubEntree();
	unsigned int i;

	Titre tab_commandes(commandes,nbcommandes);
	string fichier_entree("");
	ChaineCar fichier_sortie("output.ps");
	string outgroup("");
	string type("");
	bool val_bootstrap(true);
	bool distancesO(false);
	bool aff_taille(true);
	unsigned int style_branche(2);
	vector<string> vect_groupe;
	

	tab_commandes.Suppr(0);
	i = 0;
	while (i < tab_commandes.size()) {
		switch (tab_commandes[i][0]) {
		case '-':
			i++;
			if (tab_commandes[i-1] == "-distances") {
				distancesO = true;
				break;
			}
			if (tab_commandes[i-1] == "-nb") {
				val_bootstrap = false;
				break;
			}
			if ((i < tab_commandes.size()) && (tab_commandes[i-1] == "-t")) {
				type = tab_commandes[i];
				i++;
				break;
			}
			if ((i < tab_commandes.size()) && (tab_commandes[i-1] == "-og")) {
				tab_commandes[i].Remplacer(",", " ");
				tab_commandes[i].Remplacer("  ", " ");
				outgroup = tab_commandes[i];
				i++;
				break;
			}
			i++;
			if ((i < tab_commandes.size()) && (i > 1) && (tab_commandes[i-2] == "-g")) {
				tab_commandes[i].Remplacer(",", " ");
				tab_commandes[i].Remplacer("  ", " ");
				vect_groupe.push_back(tab_commandes[i-1]); //couleur en Anglais
				vect_groupe.push_back(tab_commandes[i]); //noms d'OTUs
				vect_groupe.push_back(""); //noms du groupe
				i++;
				break;
			}
			break;
		default:
			if (fichier_entree == "") fichier_entree = tab_commandes[i];
			else fichier_sortie = tab_commandes[i];
			tab_commandes.Suppr(i);
			break;
		}
	}

	if (fichier_entree != "") {

		//creation de l'objet
//		_Pdessin_arbre = new PhylogramTreePlotter();
		_Pdessin_arbre = new TreePlotter();

		//r�glages des options
		if (outgroup != "") {
			flux_infos << _("defining outgroup: ") << outgroup << endl;
		}
		if (val_bootstrap == false) {
			flux_infos << _("bootstrap values will not be displayed")<< endl;
		}
		if (type == "rect") {
			flux_infos << _("drawing rectangular cladogram")<< endl;
			aff_taille = false;
		}
		if (type == "slanted") {
			flux_infos << _("drawing slanted cladogram")<< endl;
			style_branche = 1;
			aff_taille = true;
		}
		if (type == "phylogram") {
			flux_infos << _("drawing phylogram")<< endl;
			aff_taille = true;
		}
				

		_fichier.open(fichier_entree.c_str(), ios::in);
	
		if (_fichier.is_open() == 0) fLiHelp();
		else {

			if (distancesO) {
				MatriceLD mat_distances;
				bool continuer (true);

				try {
					mat_distances.iFichier(_fichier);
				}
				catch (MatriceLD::Anomalie erreur) {
					cerr << erreur.fmessage(erreur.le_pb) << endl;
					exit (0);
				}
				while (continuer) {
					try {
						continuer = false;
						_Pdessin_arbre->iDistances(mat_distances);
					}
					catch (Arbre::Anomalie erreur) {
						switch(erreur.le_pb) {
						case 4:
							mat_distances.oExcel(cout);
							cout << _("This is not a square matrix") << endl;
							exit (0);
							break;
						case 6:
							cout << _("Negative distances in the matrix...") << endl;
							cout << _("the smallest value is: ") << mat_distances.get_ppvaleur()  << endl;
							cout << _("to build a tree, you must set negative values to zero.") << endl;
							if (DemandeOuiNon(_("Do you want to set negative values to zero (Y/N) ?"))) {
								mat_distances.f_neg2zero();
								continuer = true;
							}
							if (continuer == false) exit (0);
							break;
						default:
							cerr << erreur.fmessage(erreur.le_pb) << " " << erreur.le_pb << endl;
							continuer = false;
							break;
						}
					}
				} // continuer
			}
			else {
				_Pdessin_arbre->iFichier(_fichier);			
			}
	
			_Pdessin_arbre->define_outgroup(outgroup);

			if (vect_groupe.size() != 0) {
				for (i=0; i < vect_groupe.size(); i+=3) {
	 				flux_infos << _("defining colored group for: ") << vect_groupe[i+1] << endl;
					_Pdessin_arbre->ajouter_groupe( vect_groupe[i+1],  vect_groupe[i], vect_groupe[i+2]);
				}
			}
			// test couleur de groupes
			//string couleur("green");
			//_Pdessin_arbre->ajouter_groupe(outgroup, couleur, outgroup);
			// fin du test

			string extension;
			int nbmot;
 			nbmot = fichier_sortie.GetNbMots(".");
			fichier_sortie.GetMot(nbmot,extension, ".");
			
//cerr << "ApplTreePlot::fLigneCommande " << extension << endl;
			if (extension == "tre") {
				ofstream sortie;

				sortie.open(fichier_sortie.c_str(), ios::out);
        _Pdessin_arbre->oFichier(sortie, 1);//format Phylip
				sortie.close();
				
			}
			else { //dessin

				_Pdessin_arbre->set_aff_bootstrap(val_bootstrap);
				_Pdessin_arbre->set_aff_taille_branches(aff_taille);
				_Pdessin_arbre->set_style_branches(style_branche);

				FILE * Pfichier;
			
				Pfichier = fopen(fichier_sortie.c_str(),"w");
				_Pdessin_arbre->oDessin(Pfichier,extension.c_str());
			}
	//		_Pdessin_arbre->oPhylip(cout);
		}
		_fichier.close();
		affPubSortie();

	}
	else fLiHelp();
	
}
