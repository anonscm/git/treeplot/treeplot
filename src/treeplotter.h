/***************************************************************************
                          treeplotter.h  -  description
                             -------------------
    begin                : Mon Mar 26 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TREEPLOTTER_H
#define TREEPLOTTER_H

#include "arbreplus.h"
#include <plotter.h>
/*
   From the base Plotter class, the BitmapPlotter, MetaPlotter, TekPlotter,
   ReGISPlotter, HPGLPlotter, FigPlotter, CGMPlotter, PSPlotter, AIPlotter,
   SVGPlotter, GIFPlotter, PNMPlotter, PNGPlotter, and XDrawablePlotter
   classes are derived.  The PNMPlotter and PNGPlotter classes are derived
   from the BitmapPlotter class, the PCLPlotter class is derived from the
   HPGLPlotter class, and the XPlotter class is derived from the
   XDrawablePlotter class.
*/

// BitmapPlotter -> 9
// MetaPlotter
// TekPlotter,
// ReGISPlotter
// HPGLPlotter  -> 4
// FigPlotter   -> 3
// CGMPlotter   -> 5
// PSPlotter  -> 1
// AIPlotter   -> 2
// SVGPlotter   -> 6
// GIFPlotter   -> 7
// PNMPlotter  -> 8
// PNGPlotter
// XDrawablePlotter
//  classes are derived.  The PNMPlotter and PNGPlotter


/**
  *@author Olivier Langella
  */
class PointPlotArbreBase;
class PointPlotArbreNoeud;
class PointPlotArbreInd;

class TreePlotter:public ArbrePlus {
public:
	TreePlotter();
	virtual ~TreePlotter();

	virtual ArbreNdOTU * new_ArbreNdOTU(const string & nom);
	virtual ArbreNdOTU * new_ArbreNdOTU(ArbreNdNoeud * PnoeudPere);
	virtual ArbreNdNoeud * new_ArbreNdNoeud(unsigned int nbbranches);
	virtual ArbreNdNoeud * new_ArbreNdNoeud(ArbreNdNoeud * PnoeudPere=0);

  /** fabrique le dessin d'un arbre dans sortie */
  virtual void oDessin(ostream & sortie, unsigned int format=1);
  virtual void oDessin(FILE *, const char *);
  virtual void oDessin(FILE *, unsigned int format=1);

	virtual void iFichier(istream &);

//	virtual PointPlotArbreBase * get_Ppoint(NoeudPBase *) const;
	
	bool get_aff_taille_branches () const;
	double get_echelleX_arbre () const {return(_echelleX_arbre);};

	void set_aff_taille_branches (bool aff);
	void set_aff_bootstrap (bool aff);
	void set_style_branches (unsigned int style) {_style_branches = style;};
//	void define_outgroup (ChaineCar outgroup);

	Plotter * _Pplotter;

	double _decolleX;
	double _decolleY;

friend class PointPlotArbreInd;
friend class PointPlotArbreNoeud;

protected: // Protected methods
  /** Dessin de l'arbre */
	void DessineArbre();
	virtual void oDessincestparti();

	virtual void f_coloriage();
	virtual void f_calculcoordonnees();
	virtual void f_ordonneind();
	virtual void f_case_espaceX(double espaceX);
	virtual void f_case_espaceY(double espaceY, char car='B');
  /** d�callage des points pour faire un espace en X */
//  virtual void f_case_espaceX(double espaceX);
	virtual PointPlotArbreNoeud * get_Ppointplotracine();

//	const ArbreP * _Parbre;

	double _espX;
	double _espY;
	double _liminfY;
	double _limsupY;
	double _uniteY;
	double _liminfX;
	double _limsupX;
	double _echelleX_arbre;
	double _unite_echelleX_arbre;


//	vector<PointPlotArbreNoeud *> _tabPpointsnoeuds;
//	vector<PointPlotArbreInd *> _tabPpointsind;

	bool _aff_taille_branches;
	bool _aff_bootstrap;
	bool _ignore_neg_value;

	unsigned int _style_branches; // 1 => trait direct 2 => trait coud� rectangulaire

};

class PointPlotArbreBase {
public:
	PointPlotArbreBase(TreePlotter *, const Couleur & lacouleur);
	virtual ~PointPlotArbreBase() {};

	virtual void set_coordy(double coordy) {_coordy = coordy;};
	virtual double get_coordy() const {return (_coordy);};
	virtual void set_coordx(double coordx) {_coordx = coordx;};
	virtual double get_coordx() const {return (_coordx);};
	virtual const Couleur & get_couleur() const {return (_couleur);};
//	virtual PointPlot f_calculcoordoneesRec(unsigned int * Pnum_ordre) {};
	virtual void f_ordonneind(unsigned int * Pnum_ordre) {};

	virtual void f_coloriage() {};
	virtual void f_calculcoordonnees() {};
	virtual void DessineEtiquette() const {};
	virtual void DessineBranche(unsigned int style) const {};
/*
	virtual double get_longueur_totale() {return(0);};

	virtual void DessineBranche(unsigned int style=1) {};
	virtual double get_taille_etiquette() const {return(0);};
	virtual void DessineEtiquette() const {};
	virtual void DessineValBootstrap() const {};

protected:
	virtual double get_taille_bootstrap() const;


	double _cumul_longueur;

	vector<double> _tabcoordy;
*/
protected:
	Couleur _couleur;
	double _coordx;
	double _coordy;
	TreePlotter * _Ptreeplot;

};

class PointPlotArbreNoeud:public PointPlotArbreBase, public ArbreNdNoeud  {
public:
	PointPlotArbreNoeud(TreePlotter * Parbre, const Couleur & lacouleur, ArbreNdNoeud * PnoeudPere=0):PointPlotArbreBase(Parbre, lacouleur),ArbreNdNoeud((ArbrePlus *) Parbre, PnoeudPere){};
	PointPlotArbreNoeud(TreePlotter * Parbre, const Couleur & lacouleur, unsigned int nbbranches):PointPlotArbreBase(Parbre, lacouleur),ArbreNdNoeud((ArbrePlus *) Parbre, nbbranches){};
	virtual ~PointPlotArbreNoeud() {};
//	virtual PointPlot f_calculcoordoneesRec(unsigned int * Pnum_ordre);
//	virtual void f_calculcoordonees() {};
//	virtual double get_taille_bootstrap() const;
//	virtual void DessineValBootstrap() const;
	virtual double get_taille_bootstrap() const;
	virtual void f_coloriage();
	virtual void f_calculcoordonnees();
	virtual void f_ordonneind(unsigned int * Pnum_ordre);
	virtual void DessineBranche(unsigned int style) const;
	virtual void DessineValBootstrap (unsigned int style) const;
protected:

};

class PointPlotArbreInd:public PointPlotArbreBase, public ArbreNdOTU {
public:
	PointPlotArbreInd(TreePlotter *, const Couleur & lacouleur, ArbreNdNoeud *);
	PointPlotArbreInd(TreePlotter *, const Couleur & lacouleur, const string &);
	virtual ~PointPlotArbreInd() {};
	virtual void f_ordonneind(unsigned int * Pnum_ordre);
	virtual void f_coloriage();
	virtual void f_calculcoordonnees();
	virtual double get_taille_etiquette() const;
	virtual void DessineEtiquette() const;
	virtual void DessineBranche(unsigned int style) const;
//	virtual double get_taille_etiquette() const;
//	virtual void DessineEtiquette() const;

protected:
	unsigned int _num_ordre;

};


#endif
