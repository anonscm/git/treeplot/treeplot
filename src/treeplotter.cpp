/***************************************************************************
                          treeplotter.cpp  -  description
                             -------------------
    begin                : Mon Mar 26 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "treeplotter.h"

TreePlotter::TreePlotter():ArbrePlus() {
	_aff_taille_branches = false;
	_aff_bootstrap = false;

	_Pplotter = NULL;
	_espX = 3000;
	_espY = 3000;
	_liminfX = 100;
	_limsupX = 100;
	_liminfY = 100;
	_limsupY = 100;

	_decolleX = 0.0023 * _espX;
	_decolleY = 0.0023 * _espX;
	_ignore_neg_value = true;
	_style_branches = 1; // trait direct

}

TreePlotter::~TreePlotter(){
	if (_Pplotter != NULL) delete _Pplotter;
}

PointPlotArbreBase::PointPlotArbreBase(TreePlotter * Parbre, const Couleur & lacouleur):_couleur(lacouleur), _Ptreeplot(Parbre)  {
	//_cumul_longueur = 0;
	
}

PointPlotArbreInd::PointPlotArbreInd(TreePlotter * Parbre, const Couleur & lacouleur, ArbreNdNoeud * PnoeudPere)
	:PointPlotArbreBase(Parbre, lacouleur),ArbreNdOTU((ArbrePlus *) Parbre, PnoeudPere) {
}

PointPlotArbreInd::PointPlotArbreInd(TreePlotter * Parbre, const Couleur & lacouleur, const string & nom)
	:PointPlotArbreBase(Parbre, lacouleur),ArbreNdOTU((ArbrePlus *) Parbre, nom) {
}

ArbreNdOTU * TreePlotter::new_ArbreNdOTU(const string & nom) {
//	_tabPpointsind.push_back(new PointPlotArbreInd(this, nom));
//	_tabPind.push_back(_tabPpointsind.back());
	_tabPind.push_back(new PointPlotArbreInd(this,_couleur_neutre, nom));

	_tabPind.back()->set_id(_tabPind.size()-1);
	return(_tabPind.back());
}

ArbreNdOTU * TreePlotter::new_ArbreNdOTU(ArbreNdNoeud * PnoeudPere) {
//	_tabPpointsind.push_back(new PointPlotArbreInd(this, PnoeudPere));
//	_tabPind.push_back(_tabPpointsind.back());
	_tabPind.push_back(new PointPlotArbreInd(this,_couleur_neutre, PnoeudPere));

	_tabPind.back()->set_id(_tabPind.size()-1);
	return(_tabPind.back());
}

ArbreNdNoeud * TreePlotter::new_ArbreNdNoeud(unsigned int nbbranches){
//	_tabPpointsnoeuds.push_back(new PointPlotArbreNoeud(this, nbbranches));
//	_tabPnoeuds.push_back(_tabPpointsnoeuds.back());
	_tabPnoeuds.push_back(new PointPlotArbreNoeud(this,_couleur_neutre, nbbranches));

	return(_tabPnoeuds.back());
}

ArbreNdNoeud * TreePlotter::new_ArbreNdNoeud(ArbreNdNoeud * PnoeudPere) {
//	_tabPpointsnoeuds.push_back(new PointPlotArbreNoeud(this, PnoeudPere));
//	_tabPnoeuds.push_back(_tabPpointsnoeuds.back());
	_tabPnoeuds.push_back(new PointPlotArbreNoeud(this,_couleur_neutre, PnoeudPere));

	return(_tabPnoeuds.back());
}

bool TreePlotter::get_aff_taille_branches() const {
	return (_aff_taille_branches);
}

void TreePlotter::set_aff_taille_branches (bool aff) {
	if (aff == false)	_aff_taille_branches = false;
	else if (_oui_taille_branches)	_aff_taille_branches = true;
}

void TreePlotter::set_aff_bootstrap (bool aff) {
	if (aff == false)	_aff_bootstrap = false;
	else if (_oui_bootstrap)	_aff_bootstrap = true;
}

void TreePlotter::oDessin(FILE * Pfichier, const char * extension){
	unsigned int format(1);

	if (strcmp(extension, "ai") == 0) format = 2;
	if (strcmp(extension, "fig") == 0) format = 3;
	if (strcmp(extension, "hpgl") == 0) format = 4;
	if (strcmp(extension, "cgm") == 0) format = 5;
	if (strcmp(extension, "svg") == 0) format = 6;
	if (strcmp(extension, "gif") == 0) format = 7;
	if (strcmp(extension, "pnm") == 0) format = 8;
	if (strcmp(extension, "bmp") == 0) format = 9;
//	if (strcmp(extension, "tre") == 0) format = 10;
//cerr << format;
	oDessin(Pfichier, format);
}

void TreePlotter::oDessin(FILE * Pfichier, unsigned int format){
	string page("a4");
  // set a Plotter parameter

	switch (format) {
	case 1:
		_Pplotter = new PSPlotter(stdin, Pfichier, stderr);
		Plotter::parampl ("PAGESIZE", (void *) page.c_str());
		break;
	case 2:
		_Pplotter = new AIPlotter(stdin, Pfichier, stderr);
		break;
	case 3:
		_Pplotter = new FigPlotter(stdin, Pfichier, stderr);
		break;
	case 4:
		_Pplotter = new HPGLPlotter(stdin, Pfichier, stderr);
		break;
	case 5:
		_Pplotter = new CGMPlotter(stdin, Pfichier, stderr);
		break;
	case 6:
		_Pplotter = new SVGPlotter(stdin, Pfichier, stderr);
		break;
	case 7:
		_Pplotter = new GIFPlotter(stdin, Pfichier, stderr);
		break;
	case 8:
		_Pplotter = new PNMPlotter(stdin, Pfichier, stderr);
		break;
	case 9:
		_Pplotter = new BitmapPlotter(stdin, Pfichier, stderr);
		break;
	}
	oDessincestparti();
}
/** fabrique le dessin d'un arbre dans sortie */
void TreePlotter::oDessin(ostream & sortie, unsigned int format){
	switch (format) {
	case 1:
		_Pplotter = new PSPlotter(cin, sortie, cerr);
		break;
	case 2:
		_Pplotter = new AIPlotter(cin, sortie, cerr);
		break;
	case 3:
		_Pplotter = new FigPlotter(cin, sortie, cerr);
		break;
	case 4:
		_Pplotter = new HPGLPlotter(cin, sortie, cerr);
		break;
	case 5:
		_Pplotter = new CGMPlotter(cin, sortie, cerr);
		break;
	case 6:
		_Pplotter = new SVGPlotter(cin, sortie, cerr);
		break;
	case 7:
		_Pplotter = new GIFPlotter(cin, sortie, cerr);
		break;
	case 8:
		_Pplotter = new PNMPlotter(cin, sortie, cerr);
		break;
	case 9:
		_Pplotter = new BitmapPlotter(cin, sortie, cerr);
		break;
	}
//	oDessincestparti();

}

void TreePlotter::iFichier(istream & fichier) {
//cerr << "TreePlotter::iFichier" << endl;
	ArbrePlus::iFichier(fichier);

	set_aff_taille_branches (true);
	set_aff_bootstrap (true);

//	cerr << endl <<  "TreePlotter::iFichier branches " << _oui_taille_branches;
//	cerr << endl <<  "TreePlotter::iFichier boot " << _oui_bootstrap;
//	f_remplivectpoints();
}

void TreePlotter::f_ordonneind(){
	unsigned int numero(0);

//cerr << "TreePlotter::f_ordonneind debut" << endl;
//cerr << get_Ppointplotracine() << endl;
//cerr << "TreePlotter::f_ordonneind debut" << endl;
	get_Ppointplotracine()->f_ordonneind(&numero);	
//cerr << "TreePlotter::f_ordonneind fin" << endl;
}

void TreePlotter::oDessincestparti(){

//cerr << "TreePlotter::oDessincestparti debut" << endl;
  if (_Pplotter->openpl () < 0)          // open Plotter
    {
      cerr << _("Couldn't open Plotter") << endl;
//      return 1;
    }

  _Pplotter->fspace (0.0, 0.0, _espX, _espY); // specify user coor system
  _Pplotter->flinewidth (0.25);       // line thickness in user coordinates
  _Pplotter->pencolorname ("black");    // path will be drawn in red
  _Pplotter->erase ();                // erase Plotter's graphics display

 // draw_c_curve (plotter, 0.0, 400.0, 0);

//	f_remplivectpoints();
	f_coloriage();
	f_calculcoordonnees();
//cerr << "TreePlotter::oDessincestparti f_calculcoordonnees fin" << endl;

	DessineArbre();
//cerr << "TreePlotter::oDessincestparti DessineArbre fin" << endl;

  if (_Pplotter->closepl () < 0)      // close Plotter
    {
      cerr << _("Couldn't close Plotter") << endl;
 //     return 1;
    }
//cerr << "TreePlotter::oDessincestparti fin" << endl;
}

/** Dessin de l'arbre */
void TreePlotter::DessineArbre(){
	unsigned int i, style_branche(_style_branches);

	// d�termination de la taille des �tiquettes maximum
	double taillelabelmax(0), taillelabel;
	for (i=0; i < _tabPind.size(); i++) {
//		taillelabel = _tabPpointsind[i]->get_taille_etiquette();
		taillelabel = ((PointPlotArbreInd *)_tabPind[i])->get_taille_etiquette();
		if (taillelabel > taillelabelmax) taillelabelmax = taillelabel;
	}
	//=> d�callage pour faire tenir les labels
	f_case_espaceX(taillelabelmax);
	//=> d�callage pour faire tenir l'�chelle
	if (_aff_taille_branches) f_case_espaceY(_uniteY);

//cerr << "PhylogramTreePlotter::DessineArbre " << style_branche;
//  _Pplotter->pencolorname ("black");    // path will be drawn in red
	for (i=0; i < _tabPind.size(); i++) {
		((PointPlotArbreInd *)_tabPind[i])->DessineBranche(style_branche);
	}
	for (i=0; i < _tabPnoeuds.size(); i++) {
		//_tabPpointsnoeuds[i]->DessineBranche(style_branche);
		((PointPlotArbreNoeud *)_tabPnoeuds[i])->DessineBranche(style_branche);
	}

	if (_aff_taille_branches) {
		//dessin de l'�chelle
		double centre(1);
		string mot_echelle;
		//centre /= 2;
		if (_unite_echelleX_arbre < 0.011) {
			_unite_echelleX_arbre = 0.01;
			mot_echelle.assign("0.01");
		}
		else if (_unite_echelleX_arbre < 0.11) {
			_unite_echelleX_arbre = 0.1;
			mot_echelle.assign("0.1");
		}
		else if (_unite_echelleX_arbre < 1.1) {
			_unite_echelleX_arbre = 1;
			mot_echelle.assign("1");
		}
		else if (_unite_echelleX_arbre < 11) {
			_unite_echelleX_arbre = 10;
			mot_echelle.assign("10");
		}
		centre = (_unite_echelleX_arbre * _echelleX_arbre) / 2;
		centre -= (_Pplotter->flabelwidth(mot_echelle.c_str())/2);
		_Pplotter->move((int)_liminfX + (int)centre,(int) _liminfY + (int)_decolleY);
		_Pplotter->alabel('l','x',mot_echelle.c_str());
		_Pplotter->fline (_liminfX, _liminfY, _liminfX + (_unite_echelleX_arbre * _echelleX_arbre),_liminfY);		
	}	
}

void TreePlotter::f_coloriage() {
	//assigne une couleur � chaque noeud en fonction des groupes
	((PointPlotArbreNoeud *)_Pracine)->f_coloriage();
}


void TreePlotter::f_calculcoordonnees() {
	//calculs des coordonn�es des points ind et points noeuds
	//par d�faut, coordonn�es pour phylogramme

	//m�thode: calculs des coord y sur les individus...
	// => calculs des coord y r�cursif pour les noeuds.
//cerr << "TreePlotter::f_calculcoordonnees debut "<< endl;

	f_ordonneind();
//cerr << "TreePlotter::f_calculcoordonnees f_ordonneind fin "<< endl;

	unsigned int nbind (get_nbind()), i;
//	double unitey;

	_uniteY = (_espY - _liminfY - _limsupY) / (nbind-1);
//	if (_aff_taille_branches) _uniteY = (_espY - _liminfY - _limsupY) / nbind;
//	else _uniteY = (_espY - _liminfY - _limsupY) / (nbind-1);

	// calcul des coordonn�es x
	// => calculer une �chelle en trouvant la branche la plus longue
	double taillemax;
	_echelleX_arbre = 0;

	if (_aff_taille_branches) taillemax = _Pracine->get_longueur_max();
	else taillemax = _Pracine->get_nbnoeuds_max();
//cerr << "TreePlotter::f_calculcoordonnees taillemax " << taillemax << endl;

	_echelleX_arbre = (_espX - _liminfX - _limsupX) / taillemax;
//cerr << "TreePlotter::f_calculcoordonnees _echelleX_arbre " << _echelleX_arbre << endl;
	_unite_echelleX_arbre = (int) ((double) 100 * (taillemax / (double) 10));
	_unite_echelleX_arbre /= (double)100;


	((PointPlotArbreNoeud *)_Pracine)->f_calculcoordonnees();
//cerr << "TreePlotter::f_calculcoordonnees milieu "<< endl;

	for (i=0; i < nbind; i++) {
		//decallage pour faire de la place � gauche
		((PointPlotArbreInd *)_tabPind[i])->set_coordx(_liminfX + ((PointPlotArbreInd *)_tabPind[i])->get_coordx());
	//	_tabPpointsind[i]->set_coordx(_liminfX + _tabPpointsind[i]->get_coordx());
		//decallage pour faire de la place � haut
		((PointPlotArbreInd *)_tabPind[i])->set_coordy(_limsupY + ((PointPlotArbreInd *)_tabPind[i])->get_coordy());
	//	_tabPpointsind[i]->set_coordy(_limsupY + _tabPpointsind[i]->get_coordy());
	}
//cerr << "TreePlotter::f_calculcoordonnees milieu 2"<< endl;
	for (i=0; i < _tabPnoeuds.size(); i++) {
		//decallage pour faire de la place � gauche
//cerr << "TreePlotter::f_calculcoordonnees milieu 2 i " << i << endl;
		((PointPlotArbreNoeud *)_tabPnoeuds[i])->set_coordx(_liminfX + ((PointPlotArbreNoeud *)_tabPnoeuds[i])->get_coordx());
		//_tabPpointsnoeuds[i]->set_coordx(_liminfX + _tabPpointsnoeuds[i]->get_coordx());
		//decallage pour faire de la place � haut
		//_tabPpointsnoeuds[i]->set_coordy(_limsupY + _tabPpointsnoeuds[i]->get_coordy());
		((PointPlotArbreNoeud *)_tabPnoeuds[i])->set_coordy(_limsupY + ((PointPlotArbreNoeud *)_tabPnoeuds[i])->get_coordy());
	}
	
//cerr << "TreePlotter::f_calculcoordonnees fin "<< endl;
}

PointPlotArbreNoeud * TreePlotter::get_Ppointplotracine() {
	unsigned int i;

//cerr << "TreePlotter::get_Ppointplotracine debut " << _Pracine << endl;
	for (i=0; i < _tabPnoeuds.size();i++) {
		if (_Pracine == _tabPnoeuds[i]) return((PointPlotArbreNoeud *) _tabPnoeuds[i]);
	}
	return(0);
}

void PointPlotArbreInd::f_ordonneind(unsigned int * Pnum_ordre) {
//cerr << "PointPlotArbreInd::f_ordonneind debut "<< endl;
	_num_ordre = *Pnum_ordre;

	*Pnum_ordre += 1;
//cerr << "PointPlotArbreInd::f_ordonneind fin "<< endl;
}

void PointPlotArbreNoeud::f_ordonneind(unsigned int * Pnum_ordre) {
	unsigned int i;
//cerr << "PointPlotArbreNoeud::f_ordonneind debut "<< endl;

	if (est_racine()) ((PointPlotArbreNoeud *)_tabPNoeuds[0])->f_ordonneind(Pnum_ordre);
	for (i=1; i < _tabPNoeuds.size();i++) {
		((PointPlotArbreNoeud *)_tabPNoeuds[i])->f_ordonneind(Pnum_ordre);
	}	
//cerr << "PointPlotArbreNoeud::f_ordonneind fin "<< endl;
}

double PointPlotArbreInd::get_taille_etiquette() const {
	return (_Ptreeplot->_Pplotter->flabelwidth(get_nom().c_str()));	
}

double PointPlotArbreNoeud::get_taille_bootstrap() const {
//	return (_Ptreeplot->_Pplotter->flabelwidth(get_val_bootstrap()));
	return(0);	
}

void PointPlotArbreInd::DessineEtiquette() const {
	string etiquette(" ");

	etiquette += get_nom();
	
	_Ptreeplot->_Pplotter->move((int)_coordx, (int)_coordy);
	_Ptreeplot->_Pplotter->alabel('l','c',etiquette.c_str());
}

void PointPlotArbreInd::f_coloriage () {
  //d�pend de la couleur du groupe de l'OTU
  //-> recherche du groupe de l'OTU

	_couleur = _Ptreeplot->get_couleur_otu((unsigned int) get_id());
//	_couleur.set_nom("blue");

	
}

void PointPlotArbreInd::f_calculcoordonnees () {
  //calcul de coordx:
	_coordx = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordx();
	if (_Ptreeplot->get_aff_taille_branches()) {
		if ((_tabLgBranches[0] > 0) || (_Ptreeplot->_ignore_neg_value == false)) {
			_coordx += (_Ptreeplot->_echelleX_arbre * _tabLgBranches[0]);
		}
	}
	else _coordx += _Ptreeplot->_echelleX_arbre;

  //calcul de coordy:
	_coordy = (_Ptreeplot->_uniteY * (_Ptreeplot->get_nbind() - 1 - _num_ordre));
}

void PointPlotArbreNoeud::f_coloriage () {
	//assigner une couleur au noeud
	unsigned int i;
	Vecteur<Couleur> _tabCouleurs;

	//=> colorier les voisins puis d�cider de la couleur de ce
	// noeud en fonction des voisins

	if (est_racine()) {
		((PointPlotArbreNoeud *)_tabPNoeuds[0])->f_coloriage();
	}
	for (i = 1; i < _tabPNoeuds.size(); i++) {
		((PointPlotArbreNoeud *)_tabPNoeuds[i])->f_coloriage();
	}

	//les voisins sont colories: choix de la couleur
	if (est_racine()) {
		for (i = 0; i < _tabPNoeuds.size(); i++) {
			_tabCouleurs.push_back(((PointPlotArbreNoeud *)_tabPNoeuds[i])->get_couleur());
		}
		for (i = 0; i < _tabCouleurs.size(); i++) {
			if (_tabCouleurs.getNbOccurence(_tabCouleurs[i]) > ((_tabPNoeuds.size())/2)) {
				_couleur = _tabCouleurs[i];
				return;
			}
		}
	}
	else {
		for (i = 1; i < _tabPNoeuds.size(); i++) {
			_tabCouleurs.push_back(((PointPlotArbreNoeud *)_tabPNoeuds[i])->get_couleur());
		}
		for (i = 0; i < _tabCouleurs.size(); i++) {
			if (_tabCouleurs.getNbOccurence(_tabCouleurs[i]) > ((_tabPNoeuds.size()-1)/2)) {
				_couleur = _tabCouleurs[i];
				return;
			}
		}
		
	}
	
//	_couleur = _Ptreeplot->get_couleur_neutre();	
}

void PointPlotArbreNoeud::f_calculcoordonnees () {
	vector<double> tabcoordy;
	unsigned int i;

//cerr << "PointPlotArbreNoeud::f_calculcoordonnees debut" << endl;
  //calcul de coordx:
	if (est_racine()) {
		_coordx = 0;
		((PointPlotArbreNoeud *)_tabPNoeuds[0])->f_calculcoordonnees();
		tabcoordy.push_back(((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordy());
	}
	else {
		_coordx = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordx();
		if (_Ptreeplot->get_aff_taille_branches()) {
			if ((_tabLgBranches[0] > 0) || (_Ptreeplot->_ignore_neg_value == false)) _coordx += (_Ptreeplot->_echelleX_arbre * _tabLgBranches[0]);
		}
		else _coordx += _Ptreeplot->_echelleX_arbre; 		
	}

	for (i = 1; i < _tabPNoeuds.size(); i++) {
		((PointPlotArbreNoeud *)_tabPNoeuds[i])->f_calculcoordonnees();
		tabcoordy.push_back(((PointPlotArbreNoeud *)_tabPNoeuds[i])->get_coordy());
	}
	
  //calcul de coordy:
	double moyenne(0);

	for (i = 0; i < tabcoordy.size(); i++) moyenne += tabcoordy[i];

	_coordy =  moyenne / tabcoordy.size();

//cerr << "PointPlotArbreNoeud::f_calculcoordonnees fin" << endl;
}

void PointPlotArbreInd::DessineBranche(unsigned int style) const {
	double sup_coordx;
	double sup_coordy;
	Couleur couleur_ndsup(((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_couleur()); //couleur du noeud sup�rieur

	sup_coordx = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordx();
	sup_coordy = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordy();
//cerr << "PointPlotArbreBase::DessineBranche "<<sup_coordx << " "  << _coordx << endl;
//cerr << endl<< "coucou " << style << endl;
	// placer la couleur du noeud
	if (_couleur.EstNom()) _Ptreeplot->_Pplotter->pencolorname(_couleur.get_nom().c_str());
	else if (_couleur.EstRGB()) _Ptreeplot->_Pplotter->pencolor(_couleur.get_rouge(), _couleur.get_vert(), _couleur.get_bleu());

	switch (style) {
	case 1: //trait direct
		_Ptreeplot->_Pplotter->line ((int)_coordx, (int)_coordy,(int)sup_coordx ,(int)sup_coordy);
		break;
	case 2: //trait coud� rectangulaire
		_Ptreeplot->_Pplotter->line ((int)_coordx, (int)_coordy,(int)sup_coordx ,(int)_coordy); //trait horizontal
		
		if (couleur_ndsup.EstNom()) _Ptreeplot->_Pplotter->pencolorname(couleur_ndsup.get_nom().c_str());
		else if (couleur_ndsup.EstRGB()) _Ptreeplot->_Pplotter->pencolor(couleur_ndsup.get_rouge(), couleur_ndsup.get_vert(), couleur_ndsup.get_bleu());
		_Ptreeplot->_Pplotter->line ((int)sup_coordx, (int)_coordy,(int) sup_coordx ,(int) sup_coordy); //trait vertical
		break;
	}

	if (_couleur.EstNom()) _Ptreeplot->_Pplotter->pencolorname(_couleur.get_nom().c_str());
	else if (_couleur.EstRGB()) _Ptreeplot->_Pplotter->pencolor(_couleur.get_rouge(), _couleur.get_vert(), _couleur.get_bleu());
	DessineEtiquette();
	_Ptreeplot->_Pplotter->pencolor(0,0,0);
}

void PointPlotArbreNoeud::DessineValBootstrap (unsigned int style) const{
	ChaineCar val_bootstrap;
	double taille;

	val_bootstrap.AjEntier((int)get_val_bootstrap());
	taille = _Ptreeplot->_Pplotter->flabelwidth(val_bootstrap.c_str());

	//affichage des valeurs de bootstrap
	switch (style) {
		case 1:
			_Ptreeplot->_Pplotter->move((int)_coordx - (int)taille -(int) _Ptreeplot->_decolleX,(int) _coordy + (int)_Ptreeplot->_decolleY);
			_Ptreeplot->_Pplotter->alabel('l','x',val_bootstrap.c_str());
				break;
		case 2:
			_Ptreeplot->_Pplotter->move((int)_coordx - (int)taille -(int)_Ptreeplot->_decolleX,(int) _coordy + (int)_Ptreeplot->_decolleY);
			_Ptreeplot->_Pplotter->alabel('l','x',val_bootstrap.c_str());
			break;
	}
}

void PointPlotArbreNoeud::DessineBranche(unsigned int style) const {
	double sup_coordx;
	double sup_coordy;
	Couleur couleur_ndsup(((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_couleur()); //couleur du noeud sup�rieur
	//trait simple
//cerr << "PointPlotArbreBase::DessineBranche Point"  << _coordx << endl;
//cerr << "PointPlotArbreBase::DessineBranche "  << _coordy << endl;
	// dessin de la valeur de bootstrap
	if (_Ptreeplot->_aff_bootstrap) DessineValBootstrap(1);

	if (est_racine()) {}
	else {
		sup_coordx = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordx();
		sup_coordy = ((PointPlotArbreNoeud *)_tabPNoeuds[0])->get_coordy();
//cerr << "PointPlotArbreBase::DessineBranche "<<sup_coordx << " "  << _coordx << endl;
//cerr << endl<< "coucou " << style << endl;
		// placer la couleur du noeud
		 if (_couleur.EstNom()) _Ptreeplot->_Pplotter->pencolorname(_couleur.get_nom().c_str());
		 else if (_couleur.EstRGB()) _Ptreeplot->_Pplotter->pencolor(_couleur.get_rouge(), _couleur.get_vert(), _couleur.get_bleu());

		switch (style) {
		case 1: //trait direct
			_Ptreeplot->_Pplotter->line ((int)_coordx, (int)_coordy,(int)sup_coordx ,(int)sup_coordy);
			break;
		case 2: //trait coud� rectangulaire
			_Ptreeplot->_Pplotter->line ((int)_coordx, (int)_coordy,(int)sup_coordx ,(int)_coordy);

			if (couleur_ndsup.EstNom()) _Ptreeplot->_Pplotter->pencolorname(couleur_ndsup.get_nom().c_str());
			else if (couleur_ndsup.EstRGB()) _Ptreeplot->_Pplotter->pencolor(couleur_ndsup.get_rouge(), couleur_ndsup.get_vert(), couleur_ndsup.get_bleu());

			_Ptreeplot->_Pplotter->line ((int)sup_coordx, (int)_coordy,(int) sup_coordx , (int)sup_coordy);
			break;
		}
		_Ptreeplot->_Pplotter->pencolor(0, 0, 0); //noir
	}	
}

/** d�callage des points pour faire un espace en X */
void TreePlotter::f_case_espaceX(double espaceX){
	double coeff_reduction((_espX - _liminfX - _limsupX - espaceX)/(_espX - _liminfX - _limsupX));	
	unsigned int i;
	double coordx;
	
	_echelleX_arbre *= coeff_reduction;
	_unite_echelleX_arbre *= coeff_reduction;

	for (i=0; i < _tabPind.size(); i++) {
		coordx = (((PointPlotArbreInd *)_tabPind[i])->get_coordx() - _liminfX) * coeff_reduction;
		((PointPlotArbreInd *)_tabPind[i])->set_coordx(coordx + _liminfX);
	}
	for (i=0; i < _tabPnoeuds.size(); i++) {
		coordx = (((PointPlotArbreNoeud *)_tabPnoeuds[i])->get_coordx() - _liminfX) * coeff_reduction;
		((PointPlotArbreNoeud *)_tabPnoeuds[i])->set_coordx(coordx + _liminfX);
	}	
}

/** d�callage des points pour faire un espace en Y */
void TreePlotter::f_case_espaceY(double espaceY, char car){
	double coeff_reduction((_espY - _liminfY - _limsupY - espaceY)/(_espY - _liminfY - _limsupY));	
	unsigned int i;
	double coordy;
	
	_uniteY *= coeff_reduction;

	for (i=0; i < _tabPind.size(); i++) {
		coordy = (((PointPlotArbreInd *)_tabPind[i])->get_coordy() - _limsupY) * coeff_reduction;
		if (car == 'H') ((PointPlotArbreInd *)_tabPind[i])->set_coordy(coordy + _limsupY); //espace en haut
		else ((PointPlotArbreInd *)_tabPind[i])->set_coordy(coordy + _limsupY + espaceY);//espace en bas		
	}
	for (i=0; i < _tabPnoeuds.size(); i++) {
		coordy = (((PointPlotArbreNoeud *)_tabPnoeuds[i])->get_coordy() - _limsupY) * coeff_reduction;
		if (car == 'H') ((PointPlotArbreNoeud *)_tabPnoeuds[i])->set_coordy(coordy + _limsupY); //espace en haut
		else ((PointPlotArbreNoeud *)_tabPnoeuds[i])->set_coordy(coordy + _limsupY + espaceY); //espace en bas
	}	
}

