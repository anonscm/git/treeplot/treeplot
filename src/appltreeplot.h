/***************************************************************************
                          appltreeplot.h  -  description
                             -------------------
    begin                : Tue Dec 19 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APPLTREEPLOT_H
#define APPLTREEPLOT_H

#include "applications.h"
//#include "phylogramtreeplotter.h"
#include "treeplotter.h"

#define version_treeplot "0.7.4"

/**Application Treeplotter
  *@author Olivier Langella
  */

class ApplTreePlot : public Application  {
public: 
	ApplTreePlot();
	~ApplTreePlot();

	void lancement(int nbcommandes, char ** commandes);
	void fLigneCommande(char ** commandes, int nbcommandes);

protected:

	void affPubSortie() const;
	void fLiHelp() const;
	void affPubEntree() const;

	TreePlotter * _Pdessin_arbre;

	ostream & flux_infos;
};

#endif

